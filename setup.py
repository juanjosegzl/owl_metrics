from distutils.core import setup

setup(name='owl_metrics',
      version='0.1',
      py_modules=['owl_metrics'],
      scripts=['scripts/get_owl_metrics.py']
      )
