from collections import Counter

import rdflib

SUBJ = 0
PRED = 1
OBJ = 2

def _get_triples(filename):
    g = rdflib.Graph()
    g.parse(filename)

    return [
        [subj.split('#')[-1],
         pred.split('#')[-1],
         obj.split('#')[-1]]
        for subj, pred, obj in g
    ]

def get_metrics(filename):
    triples = _get_triples(filename)

    preds = [t[PRED] for t in triples]
    objs = [t[OBJ] for t in triples]

    count_preds = Counter(preds)
    count_objs = Counter(objs)

    counts = {
        'classes': sum([
            1
            for t in triples
            if t[PRED] == 'type'
            and t[OBJ] == 'Class'
            and len(t[SUBJ]) != len('Nc0c35ab5c87545cab87e42c816b2aabe')
        ]),
        'disjoint_classes': sum([
            1
            for t in triples
            if t[PRED] == 'disjointWith'
            or (t[PRED] == 'type' and t[OBJ] == 'AllDisjointClasses')
        ]),
        'sub_classes': count_preds.get('subClassOf', 0),
        'equivalent_classes': count_preds.get('equivalentClass', 0),
        'individuals': count_objs.get('NamedIndividual', 0),
        'object_properties': count_objs.get('ObjectProperty', 0),
        'data_properties': count_objs.get('DatatypeProperty', 0),
        'domains': count_preds.get('domain', 0),
        'ranges': count_preds.get('range', 0),
        'inverse_object_properties': count_preds.get('inverseOf', 0),
        'functional_object_properties': count_objs.get('FunctionalProperty', 0),
        'inverse_functional_object_properties': count_objs.get('InverseFunctionalProperty', 0),
        'transitive_object_properties': count_objs.get('TransitiveProperty', 0),
        'symmetric_object_properties': count_objs.get('SymmetricProperty', 0),
        'asymmetric_object_properties': count_objs.get('AsymmetricProperty', 0),
        'reflexive_object_properties': count_objs.get('ReflexiveProperty', 0),
        'irreflexive_object_properties': count_objs.get('IrreflexiveProperty', 0),
        'existential_value_restrictions': count_preds.get('someValuesFrom', 0),
        'universal_value_restrictions': count_preds.get('allValuesFrom', 0),
        'conjunction_groups': count_preds.get('intersectionOf', 0),
        'disjunction_groups': count_preds.get('unionOf', 0),
        'sub_object_properties': count_preds.get('subPropertyOf', 0),
        'nominals': count_preds.get('hasValue', 0),
        'min_cardinalities': count_preds.get('minCardinality', 0) + count_preds.get('minQualifiesCardinality', 0),
        'max_cardinalities': count_preds.get('maxCardinality', 0) + count_preds.get('maxQualifiedCardinality', 0)
    }

    ratio_denominator = sum((
        counts['min_cardinalities'],
        counts['universal_value_restrictions'],
        counts['disjunction_groups'],
        counts['conjunction_groups'],
        counts['max_cardinalities'],
        counts['existential_value_restrictions']
    ))

    if ratio_denominator == 0:
        counts['ratio_min_universal'] = 0.0
        counts['ratio_max_existential'] = 0.0
        counts['ratio_disjunction'] = 0.0
        counts['ratio_conjunction'] = 0.0
    else:
        counts['ratio_min_universal'] = sum(
            (counts['min_cardinalities'],
             counts['universal_value_restrictions'])
        ) / ratio_denominator
        counts['ratio_max_existential'] = sum(
            (counts['max_cardinalities'],
             counts['existential_value_restrictions'])
        ) / ratio_denominator
        counts['ratio_disjunction'] = (counts['disjunction_groups'] /
                                       ratio_denominator)
        counts['ratio_conjunction'] = (counts['conjunction_groups'] /
                                       ratio_denominator)

    return counts
