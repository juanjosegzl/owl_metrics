import io
import csv
import multiprocessing
import os
import queue
from multiprocessing import Queue
import sys

import click

from owl_metrics import get_metrics

class CSVWriter:

    def __init__(self, print_header=False):
        self.writer = None
        self.print_header = print_header
        self.out = None

    def _init_writer(self, metrics):
        fieldnames = sorted(metrics.keys())
        self.out = io.StringIO()
        self.writer = csv.DictWriter(self.out, fieldnames=fieldnames, restval=0)

        if self.print_header:
            self.writer.writeheader()

    def append_row(self, metrics):
        if self.writer is None:
            self._init_writer(metrics)

        self.writer.writeheader()
        self.writer.writerow(metrics)

    def __str__(self):
        return self.out.getvalue()


q = Queue()

def worker(ontology):
    try:
        metrics = get_metrics(ontology)
        metrics['ontology'] = ontology.split('/')[-1]
        writer = CSVWriter()
        writer.append_row(metrics)
        print(writer,end="")
    except Exception as err:
        print(err, file=sys.stderr)

@click.command()
@click.argument('filenames', type=click.Path(exists=True), nargs=-1)
@click.option('--header/--no-header', default=False)
def main(filenames, header):
    '''Simple program that counts metrics of OWL ontologies.'''

    ontologies = []
    for filename in filenames:
        if os.path.isdir(filename):
            ontologies.extend([os.path.join(filename, f) for f in os.listdir(filename)])
        else:
            ontologies.append(filename)

    with multiprocessing.Pool(4) as p:
        for i, _ in enumerate(p.imap_unordered(worker, ontologies), 1):
            sys.stderr.write(f"\r{i} of {len(ontologies)} ({i/len(ontologies):.1%})")

    # writer = CSVWriter(print_header=header)
    # i = 1
    # while not q.empty():
    #     sys.stderr.write(f"\r {i} of {len(ontologies)}")
    #     i += 1
    #     writer.append_row(q.get())

    # print(writer)


if __name__ == '__main__':
    main()
