init:
	pipenv install
	pipenv shell

q:
	pipenv run pycodestyle owl_metrics.py

.PHONY: init
